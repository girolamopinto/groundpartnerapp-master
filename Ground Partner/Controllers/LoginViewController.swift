//
//  LoginViewController.swift
//  Ground Partner
//
//  Created by Pasquale Vittoriosi on 06/05/2020.
//  Copyright © 2020 PaperPlanes. All rights reserved.
//

import UIKit
import BarefootKit
import LocalAuthentication

class LoginViewController: UIViewController {
	
	//MARK:- Outlets
	@IBOutlet weak var usernameTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
	//MARK:- Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		//  Check if a Username is saved in UserDefaults.
		if let storedUsername = UserDefaults.standard.string(forKey: "Username") {
			//  The Username is Stored.
			usernameTextField.text = storedUsername
			//  Retriving saved password in the Keychain.
			let context = LAContext()
			context.localizedCancelTitle = "Enter Username/Pin Manualy"
			let reason = "Log in to your account"
			//  Request Keychain Access.
			context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
				//  Request the Password stored in Keychain.
				if success {
					do {
						let passwordItem = KeychainItem(service: KeychainConfiguration.serviceName,
														account: storedUsername,
														accessGroup: KeychainConfiguration.accessGroup)
						let keychainPassword = try passwordItem.readPassword()
						DispatchQueue.main.async {
							self.view.setActivityIndicator()
							self.passwordTextField.text = keychainPassword
						}
						//  Request a valid Token for the Database (Authenticates the user).
						Database.shared.refreshToken(for: storedUsername, password: keychainPassword) { (token, expiry, code) in
							
							guard let _ = token else {
								//  Request Failed.
								DispatchQueue.main.async {
									self.view.removeActivityIndicator()
								}
								//  There is an error.
								switch code {
									case "-1009":
										DispatchQueue.main.async {
											let alertController = UIAlertController(title: "No Internet Connection", message: "Internet Connection is required to enter Dashboard.", preferredStyle: .alert)
											alertController.addAction(UIAlertAction(title: "OK", style: .cancel,handler: { action in
												self.view.removeActivityIndicator()
											}))
											self.present(alertController, animated: true, completion: nil)
									}
									default:
										DispatchQueue.main.async {
											let alertController = UIAlertController(title: "Error", message: "\(code)", preferredStyle: .alert)
											alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel,handler: { action in
												self.view.removeActivityIndicator()
												self.passwordTextField.becomeFirstResponder()
											}))
											self.present(alertController, animated: true, completion: nil)
									}
								}
								return
							}
							//  Request Successful.
							DispatchQueue.main.async {
								self.view.removeActivityIndicator()
							}
							self.performSegue(withIdentifier: "GoToHome", sender: self)
						}
					}
					catch {
						debugPrint("Error reading password from Keychain: \(error)")
						DispatchQueue.main.async {
							// Return to the TextField to enter the password.
							self.passwordTextField.becomeFirstResponder()
						}
					}
				} else {
					debugPrint(error?.localizedDescription ?? "Error accessing the Keychain.")
					DispatchQueue.main.async {
						// Return to the TextField to enter the password.
						self.passwordTextField.becomeFirstResponder()
					}
				}
			}
		}
		
	}
	
	//  End the editing and hides the keyboard if the User was editing the TextFields and taps everywhere in the View.
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		self.view.endEditing(true)
	}
	
	//MARK:- Actions
	@IBAction func loginButtonPressed(_ sender: Any) {
		guard let username = self.usernameTextField.text,
			let password = self.passwordTextField.text,
			!username.isEmpty,
			!password.isEmpty else {
				return
		}
		DispatchQueue.main.async {
			self.view.setActivityIndicator()
		}
		//  Request a valid Token for the Database (Authenticates the user).
		Database.shared.refreshToken(for: username, password: password) { (token, _, code) in
			guard let _ = token else {
				//  Request Failed.
				DispatchQueue.main.async {
					self.view.removeActivityIndicator()
				}
				//  No Internet Connection.
				if code == "-1009" {
					//  Check if the user already did a valid access.
					if username == UserDefaults.standard.string(forKey: "Username") {
						do {
							//  Read the password on the Keychain.
							let passwordItem = KeychainItem(service: KeychainConfiguration.serviceName,
															account: username,
															accessGroup: KeychainConfiguration.accessGroup)
							let keychainPassword = try passwordItem.readPassword()
							
							//  Check if the password in the Password Textfield is the same on the Keychain.
							if password == keychainPassword {
								//  The User already did a valid Login and is authenticated with the same Password on the Keychain so it's possible to enter into Offline Mode.
								DispatchQueue.main.async {
									let alertController = UIAlertController(title: "No Internet Connection", message: "Going into offline Mode", preferredStyle: .alert)
									alertController.addAction(UIAlertAction(title: "OK", style: .cancel,handler: { action in
										self.view.removeActivityIndicator()
										self.performSegue(withIdentifier: "GoToHome", sender: self)
									}))
									self.present(alertController, animated: true, completion: nil)
								}
							} else {
								//  The User already did a valid Login but is NOT authenticated through Keychain.
								DispatchQueue.main.async {
									let alertController = UIAlertController(title: "Wrong Password", message: "Wrong password", preferredStyle: .alert)
									alertController.addAction(UIAlertAction(title: "OK", style: .cancel,handler: { action in
										self.view.removeActivityIndicator()
										// Return to the TextField to enter the correct password.
										self.passwordTextField.becomeFirstResponder()
									}))
									self.present(alertController, animated: true, completion: nil)
								}
							}
						} catch {
							debugPrint("Error reading password from keychain: \(error)")
						}
					} else {
						//  The User never did a valid Login and is NOT possible to authenticate with the Keychain so it's  NOT possible to enter into Offline Mode.
						DispatchQueue.main.async {
							let alertController = UIAlertController(title: "No Internet Connection", message: "Impossible to go in offline mode. You must have loggedin at leats once!", preferredStyle: .alert)
							alertController.addAction(UIAlertAction(title: "OK", style: .cancel,handler: { action in
								DispatchQueue.main.async {
									self.view.removeActivityIndicator()
								}
							}))
							self.present(alertController, animated: true, completion: nil)
						}
					}
				}
				return
			}
			//  Request Successful.
			//  Checks if it's a new User.
			if username != UserDefaults.standard.string(forKey: "Username") {
				// New User with a valid access on the Database.
				// Remove any OLD value iof present.
				UserDefaults.standard.removeObject(forKey: "UserRecordID")
				UserDefaults.standard.removeObject(forKey: "UserID")
				//  Save the NEW User.
				UserDefaults.standard.set(username, forKey: "Username")
				do {
					// This is a new account, create a new Keychain item with the account name.
					let passwordItem = KeychainItem(service: KeychainConfiguration.serviceName,
													account: username,
													accessGroup: KeychainConfiguration.accessGroup)
					// Save the password for the new User.
					try passwordItem.savePassword(password)
				} catch {
					DispatchQueue.main.async {
						self.view.removeActivityIndicator()
					}
					debugPrint("Error saving the Password on the Keychain: \(error)")
				}
			}
			DispatchQueue.main.async {
				self.view.removeActivityIndicator()
			}
			self.performSegue(withIdentifier: "GoToHome", sender: self)
		}
	}
	
	
}

//MARK:- UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		//  Resign the responder.
		switch textField {
			// The User returned from the Username TextField.
			case usernameTextField:
				// The User needs to enter the Password.
				passwordTextField.becomeFirstResponder()
			default:
				// Hides the Keyboard.
				textField.endEditing(true)
		}
		return true
	}
}
