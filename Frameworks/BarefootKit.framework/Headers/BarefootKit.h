//
//  BarefootKit.h
//  BarefootKit
//
//  Created by Pasquale Vittoriosi on 21/01/2020.
//  Copyright © 2020 Pasquale Vittoriosi. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BarefootKit.
FOUNDATION_EXPORT double BarefootKitVersionNumber;

//! Project version string for BarefootKit.
FOUNDATION_EXPORT const unsigned char BarefootKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BarefootKit/PublicHeader.h>


